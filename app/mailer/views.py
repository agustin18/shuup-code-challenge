from django.views.generic import ListView

from mailer.models import Company

from django.db.models import Sum, DecimalField


class IndexView(ListView):
    template_name = "mailer/index.html"
    model = Company
    queryset = Company.objects.all().prefetch_related('orders', 'contacts', 'contacts__orders').annotate(sum=Sum('orders__total', output_field=(DecimalField(max_digits=20, decimal_places=2))))
    paginate_by = 100
